﻿Public Class Validaciones
    Private _usuario, _contra, _pregunta, _respuesta, _tipousuario, _nivel, _repetircontra As String
    Dim hora() As String = {"7:00 AM", "6:00 PM", "8:30 AM", "8:50 AM", "10:20 AM", "10:45 AM", "11:30 AM", "12:05 PM", "12:55 PM", "2:25 PM", "2:45 PM", "4:15 PM", "4:30 PM"}
    Public Property AsignarRepetirContra() As String
        Set(ByVal value As String)
            _repetircontra = value
        End Set
        Get
            Return _repetircontra
        End Get
    End Property

    Public Property AsignarNivel() As String
        Set(ByVal value As String)
            _nivel = value
        End Set
        Get
            Return _nivel
        End Get
    End Property

    Public Property AsignarTipoUsuario() As String
        Set(ByVal value As String)
            _tipousuario = value
        End Set
        Get
            Return _tipousuario
        End Get
    End Property

    Public Property AsignarRespuesta() As String
        Set(ByVal value As String)
            _respuesta = value
        End Set
        Get
            Return _respuesta
        End Get
    End Property

    Public Property AsignarPregunta() As String
        Set(ByVal value As String)
            _pregunta = value
        End Set
        Get
            Return _pregunta
        End Get
    End Property

    Public Property AsignarUsuario() As String
        Set(ByVal value As String)
            _usuario = value
        End Set
        Get
            Return _usuario
        End Get
    End Property

    Public Property AsignarContra() As String
        Set(ByVal value As String)
            _contra = value
        End Set
        Get
            Return _contra
        End Get
    End Property


    Public Function ValidarComboBox(ByVal cmb1 As ComboBox, ByVal cmb2 As ComboBox, ByVal cmb3 As ComboBox)
        If cmb1.SelectedItem Is Nothing Or cmb2.SelectedItem Is Nothing Or cmb3.SelectedItem Is Nothing Then
            Return MsgBox("Debes seleccionar una opción", MsgBoxStyle.Critical)
        Else
            Return True
        End If
    End Function

    Public Function ValidarComboBox(ByVal cmb1 As ComboBox)
        If cmb1.SelectedItem Is Nothing Then
            Return MsgBox("Debes seleccionar una opción", MsgBoxStyle.Critical)
        Else
            Return True
        End If
    End Function

    Public Sub LimpiarTextBox(ByVal txt As TextBox)
        txt.Clear()
    End Sub

    Public Sub LimpiarComboBox(ByVal cmb As ComboBox)
        cmb.Items.Clear()
    End Sub

    Public Function ValidarUsuario()
        If _usuario.Length = 0 Then
            Return MsgBox("El campo usuario está vacío", MsgBoxStyle.Critical)
        ElseIf _usuario.Length < 5 Then
            Return MsgBox("El usuario no puede tener menos de 5 caracteres")
        ElseIf _usuario.Length > 50 Then
            Return MsgBox("El usuario no puede tener más de 50 caracteres")
        Else
            Return True
        End If
    End Function

    Public Function ValidarUsuarioEntrar()
        If _usuario.Length = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function ValidarContraEntrar()
        If _contra.Length = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function ValidarContra()
        If _contra.Length = 0 Or _repetircontra.Length = 0 Then
            Return MsgBox("Introduzca una contraseña")
        ElseIf _contra.Length < 5 Or _repetircontra.Length < 5 Then
            Return MsgBox("Debe introucir más carácteres en la contraseña")
        ElseIf _contra.Length > 50 Or _repetircontra.Length > 50 Then
            Return MsgBox("La cantidad máxima de caracteres para la contraseña es 50", MsgBoxStyle.Information)
        ElseIf _contra <> _repetircontra Then
            Return MsgBox("Las contraseñas no coinciden", MsgBoxStyle.Critical)
        Else
            Return True
        End If
    End Function
    Public Function ValidarRespuesta()
        If _respuesta.Length = 0 Then
            Return MsgBox("No puede dejar la respuesta vacía", MsgBoxStyle.Critical)
        ElseIf _respuesta.Length > 100 Then
            Return MsgBox("No puede introducir más de 100 caracteres en la respuesta", MsgBoxStyle.Critical)
        Else
            Return True
        End If
    End Function

    Public Sub ValidarHoras(ByVal dtp1 As DateTimePicker, e As KeyEventArgs)
        If e.KeyCode = Keys.Up And dtp1.Value = Convert.ToDateTime(hora(2)) Then
            dtp1.Value = dtp1.Value.AddMinutes(20)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Up And dtp1.Value = Convert.ToDateTime(hora(4)) Then
            dtp1.Value = dtp1.Value.AddMinutes(25)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Up And dtp1.Value = Convert.ToDateTime(hora(6)) Then
            dtp1.Value = dtp1.Value.AddMinutes(35)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Up And dtp1.Value = Convert.ToDateTime(hora(7)) Then
            dtp1.Value = dtp1.Value.AddMinutes(50)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Up And dtp1.Value = Convert.ToDateTime(hora(9)) Then
            dtp1.Value = dtp1.Value.AddMinutes(20)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Up And dtp1.Value = Convert.ToDateTime(hora(11)) Then
            dtp1.Value = dtp1.Value.AddMinutes(15)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Up And dtp1.Value < dtp1.MaxDate Then
            dtp1.Value = dtp1.Value.AddMinutes(45)
            e.Handled = True
        End If

        If e.KeyCode = Keys.Down And dtp1.Value = Convert.ToDateTime(hora(3)) Then
            dtp1.Value = dtp1.Value.AddMinutes(-20)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Down And dtp1.Value = Convert.ToDateTime(hora(5)) Then
            dtp1.Value = dtp1.Value.AddMinutes(-25)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Down And dtp1.Value = Convert.ToDateTime(hora(7)) Then
            dtp1.Value = dtp1.Value.AddMinutes(-35)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Down And dtp1.Value = Convert.ToDateTime(hora(8)) Then
            dtp1.Value = dtp1.Value.AddMinutes(-50)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Down And dtp1.Value = Convert.ToDateTime(hora(10)) Then
            dtp1.Value = dtp1.Value.AddMinutes(-20)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Down And dtp1.Value = Convert.ToDateTime(hora(12)) Then
            dtp1.Value = dtp1.Value.AddMinutes(-15)
            e.Handled = True
        ElseIf e.KeyCode = Keys.Down And dtp1.Value > dtp1.MinDate Then
            dtp1.Value = dtp1.Value.AddMinutes(-45)
            e.Handled = True
        End If
    End Sub
End Class