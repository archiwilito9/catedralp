﻿Imports MySql.Data.MySqlClient
Public Class Conexion
    Private cadena As String
    Public cn As New MySqlConnection
    Public da As MySqlDataAdapter
    Public cm As MySqlCommand
    Public ds As DataSet
    Public Sub New(ByVal server As String, ByVal user As String, ByVal password As String, ByVal bdd As String)
        cadena = "server=" & server & "; user id=" & user & "; password='" & password & "'; database=" & bdd & ";" 'cadena de conexion
        cn = New MySqlConnection(cadena) 'creamos la conexion
    End Sub
    Public Sub conectarse()
        'Verificamos primero sí la coneccion esta cerrada
        If cn.State = ConnectionState.Closed Then
            cn.Open()
        Else
            'Se genera un mensaje de error si la conexión ya estaba abierta
            Throw New Exception("La conexión ya esta establecida")
        End If
    End Sub
    Public Sub desconectarse()
        'Verificamos primero sí la coneccion esta abierta
        If cn.State = ConnectionState.Open Then
            cn.Close()
        Else
            'Se genera un mensaje de error si la conexión ya estaba cerrada
            Throw New Exception("La conexión ya esta cerrada")
        End If
    End Sub
    Public Sub llenar_dgvEliminar(ByRef dgv As DataGridView, ByVal consulta As String, ByVal tabla As String)
        Try
            conectarse() 'conectarse
            'Creamos el datadapter que ejecuta la consulta en la bdd
            da = New MySqlDataAdapter(consulta, cn)
            'Creamos en nuevo dataset para alojar los datos del da
            ds = New DataSet
            'Ejecutamos el método Fill para llenar el ds con los datos arrojados por la consulta
            da.Fill(ds, tabla)
            'Establecemos que el DataGridView se llena con los datos de la tabla alojada en el ds
            dgv.DataSource = ds.Tables(tabla)
            'Nos desconectamos
            Dim btn As New DataGridViewButtonColumn
            dgv.Columns.Add(btn)
            btn.HeaderText = "Eliminar"
            btn.Text = "Eliminar"
            btn.Name = "btn"
            btn.UseColumnTextForButtonValue = True
            desconectarse()
        Catch ex As Exception
            'Se muesta cualquier error que se haya dado en la ejecuciónd e la consulta
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub llenar_dgvEditar(ByRef dgv As DataGridView, ByVal consulta As String, ByVal tabla As String)
        Try
            conectarse()
            da = New MySqlDataAdapter(consulta, cn)
            ds = New DataSet
            da.Fill(ds, tabla)
            dgv.DataSource = ds.Tables(tabla)
            Dim btn As New DataGridViewButtonColumn
            dgv.Columns.Add(btn)
            btn.HeaderText = "Editar"
            btn.Text = "Editar"
            btn.Name = "btn"
            btn.UseColumnTextForButtonValue = True
            desconectarse()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub llenar_dgvDesocuparSalon(ByRef dgv As DataGridView, ByVal consulta As String, ByVal tabla As String)
        Try
            conectarse()
            da = New MySqlDataAdapter(consulta, cn)
            ds = New DataSet
            da.Fill(ds, tabla)
            dgv.DataSource = ds.Tables(tabla)
            Dim btn As New DataGridViewButtonColumn
            dgv.Columns.Add(btn)
            btn.HeaderText = "Desocupar"
            btn.Text = "Desocupar"
            btn.Name = "btn"
            btn.UseColumnTextForButtonValue = True
            desconectarse()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Sub llenar_cbo(ByRef cbo As ComboBox, ByVal consulta As String, ByVal tabla As String)
        Try
            conectarse() 'conectarse
            'Creamos el datadapter que ejecuta la consulta en la bdd
            da = New MySqlDataAdapter(consulta, cn)
            'Creamos en nuevo dataset para alojar los datos del da
            ds = New DataSet
            'Ejecutamos el método Fill para llenar el ds con los datos arrojados por la consulta
            da.Fill(ds, tabla)
            'Establecemos que el ComboBox se llena con los datos de la tabla alojada en el ds
            cbo.DataSource = ds.Tables(tabla)
            'En la propiedad ValueMember se estableceel valor del campo de la tabla 
            'que se devolverá con el método SelectedValue
            cbo.ValueMember = ds.Tables(tabla).Columns(0).ToString
            'En la propiedad DisplayMember se estableceel valor del campo de la tabla 
            'que se mostrará en el texto del Combo
            'cbo.DisplayMember = ds.Tables(tabla).Columns(1).ToString
            'Nos desconectamos
            desconectarse()
        Catch ex As Exception
            'Se muesta cualquier error que se haya dado en la ejecuciónd e la consulta
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Sub ejecutar(ByVal consulta As String)
        Try
            conectarse() 'conectarse
            'Se crea el comando con la consulta y la conexion
            cm = New MySqlCommand(consulta, cn)
            'Se ejecuta
            cm.ExecuteNonQuery()
            desconectarse() 'conectarse
        Catch ex As Exception
            'Se muesta cualquier error que se haya dado en la ejecuciónd e la consulta
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Function campo(ByVal consulta As String) As String
        Try
            conectarse() 'conectarse
            'Se crea el comando con la consulta y la conexion
            cm = New MySqlCommand(consulta, cn)
            campo = cm.ExecuteScalar
            desconectarse()
        Catch ex As Exception
            'Se muesta cualquier error que se haya dado en la ejecuciónd e la consulta
            Return MsgBox(ex.ToString)
        End Try
    End Function

End Class
