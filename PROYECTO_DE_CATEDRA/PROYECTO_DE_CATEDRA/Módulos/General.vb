﻿Module General
    Public Sub ControlaEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs, ByVal controles As Form.ControlCollection)
        If e.KeyChar = Convert.ToChar(Keys.Return) Then
            Dim indice As Integer = sender.TabIndex + 1
            For Each i As Control In controles
                'For i As Integer = 0 To control.Length - 1
                If i.TabIndex = indice Then
                    i.Focus()
                    Exit Sub
                End If
            Next i
        End If
    End Sub
End Module
