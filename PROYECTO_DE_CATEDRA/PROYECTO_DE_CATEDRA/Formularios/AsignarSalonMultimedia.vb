﻿Public Class AsignarSalonMultimedia
    Dim conectar As New Conexion("localhost", "root", "", "proy_lp_catedra")
    Dim validar As New Validaciones
    Dim consulta As String = "SELECT usuario FROM usuarios"
    Dim hora_inicio, hora_fin, fecha, diferencia, id_reservante, id_salon, persona_reservante As String
    Dim consulta1, consulta2 As String
    Dim dtpinicioval As String = "7:00 AM"
    Dim dtpfinalval As String = "6:00 PM"
    Dim hora1 As String = "10:45 AM", hora2 As String = "12:05 PM"

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        GestionarSalonMultimedia.Show()
        Me.Close()
    End Sub

    Private Sub AsignarSalonMultimedia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        conectar.llenar_cbo(ComboBox2, "SELECT nombre FROM salones", "salones")
        conectar.llenar_cbo(cmbreservante, consulta, "usuarios")
        'Asignar fecha mínima al dtp
        dtpdia.MinDate = Today
        'Asginar valor de hora al dtp desde
        dtpdesde.Value = Convert.ToDateTime(dtpinicioval)
        'Asignar la hora mínima al dtp desde
        dtpdesde.MinDate = Convert.ToDateTime(dtpinicioval)
        'Asignar la hora máxima al dtp desde
        dtpdesde.MaxDate = Convert.ToDateTime(dtpfinalval)
        'Asginar valor de hora al dtp hasta
        dtphasta.Value = Convert.ToDateTime(dtpinicioval)
        'Asignar la hora mínima al dtp hasta
        dtphasta.MinDate = Convert.ToDateTime(dtpinicioval)
        'Asignar la hora máxima al dtp hasta
        dtphasta.MaxDate = Convert.ToDateTime(dtpfinalval)
        'dtpdesde.ma
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        fecha = String.Format("{0:yyyy-MM-dd}", dtpdia.Value)
        hora_inicio = String.Format("{0:HH:mm:ss}", dtpdesde.Value)
        hora_fin = String.Format("{0:HH:mm:ss}", dtphasta.Value)
        diferencia = DateDiff(DateInterval.Minute, dtpdesde.Value, dtphasta.Value)
        If diferencia = 45 Or diferencia = 90 Then
            id_reservante = conectar.campo("SELECT id_usuario FROM usuarios WHERE usuario = '" & cmbreservante.SelectedValue & "'")
            id_salon = conectar.campo("SELECT id_salones FROM salones WHERE nombre = '" & ComboBox2.SelectedValue & "'")
            persona_reservante = conectar.campo("SELECT usuario FROM usuarios WHERE id_usuario = '" & id_reservante & "'")
            conectar.ejecutar("INSERT INTO reserva (fecha,hora_inicio,hora_fin,id_reservante,persona_reservante,id_salon) VALUES ('" & fecha & "', '" & hora_inicio & "', '" & hora_fin & "', '" & id_reservante & "', '" & persona_reservante & "', '" & id_salon & "')")
            MsgBox("Reservado " & diferencia & " minutos, " & vbCrLf & "el día " & fecha)
        ElseIf dtpdesde.Value = Convert.ToDateTime(hora1) And dtphasta.Value = Convert.ToDateTime(hora2) And diferencia = 80 Then
            MsgBox("Reservado " & diferencia & " minutos, " & vbCrLf & "el día " & fecha)
        ElseIf dtpdesde.Value = Convert.ToDateTime("11:30 AM") And dtphasta.Value = Convert.ToDateTime("12:05 PM") And diferencia = 35 Then
            MsgBox("Reservado " & diferencia & " minutos, " & vbCrLf & "el día " & fecha)
        Else
            MsgBox("La horas son incorrectas, son 45 ó 90 minutos." & vbCrLf & "Los minutos ingresados fueron: " & diferencia, MsgBoxStyle.Critical)

        End If
    End Sub

    Private Sub dtpdesde_KeyDown(sender As Object, e As KeyEventArgs) Handles dtpdesde.KeyDown
        validar.ValidarHoras(dtpdesde, e)
    End Sub

    Private Sub dtphasta_KeyDown(sender As Object, e As KeyEventArgs) Handles dtphasta.KeyDown
        validar.ValidarHoras(dtphasta, e)
    End Sub
End Class