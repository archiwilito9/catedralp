﻿Public Class DesocuparSalonMultimedia
    Dim conectar As New Conexion("localhost", "root", "", "proy_lp_catedra")
    Dim borrar, consulta_borrar As String
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        GestionarSalonMultimedia.Show()
        Me.Close()
    End Sub

    Private Sub DesocuparSalonMultimedia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        conectar.llenar_dgvDesocuparSalon(dgvDesocuparSalon, "SELECT * FROM reserva", "reserva")
    End Sub

    Private Sub dgvDesocuparSalon_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDesocuparSalon.CellClick
        If e.ColumnIndex = 7 Then
            borrar = dgvDesocuparSalon.Rows(e.RowIndex).Cells(0).Value.ToString
            consulta_borrar = "DELETE FROM reserva WHERE id_reserva='" & borrar & "'"
            If MsgBox("Está seguro de desocupar este salón?", MsgBoxStyle.YesNo) = vbYes Then
                conectar.campo(consulta_borrar)
                MsgBox("La reserva fue eliminada con éxito", MsgBoxStyle.Information)
                dgvDesocuparSalon.Rows.RemoveAt(e.RowIndex)
            End If
        End If
    End Sub
End Class