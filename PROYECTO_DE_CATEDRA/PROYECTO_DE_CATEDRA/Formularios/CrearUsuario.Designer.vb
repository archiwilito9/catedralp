﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CrearUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.cmbnivel = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtusuario = New System.Windows.Forms.TextBox()
        Me.txtcontra = New System.Windows.Forms.TextBox()
        Me.txtrepetircontra = New System.Windows.Forms.TextBox()
        Me.txtrespuesta = New System.Windows.Forms.TextBox()
        Me.cmbtipousuario = New System.Windows.Forms.ComboBox()
        Me.cmbpregunta = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnfinalizar = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.068602!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93.9314!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(407, 379)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Location = New System.Drawing.Point(5, 0)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(397, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Completa los siguientes datos:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.66085!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.33915!))
        Me.TableLayoutPanel2.Controls.Add(Me.Button2, 0, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbnivel, 1, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.Label2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label3, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label4, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label6, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.Label7, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.txtusuario, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtcontra, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.txtrepetircontra, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.txtrespuesta, 1, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbtipousuario, 1, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbpregunta, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label8, 0, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.btnfinalizar, 1, 7)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 26)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 8
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.68085!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55.31915!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(401, 350)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Teal
        Me.Button2.BackgroundImage = Global.PROYECTO_DE_CATEDRA.My.Resources.Resources.back_arrow
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Button2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button2.Location = New System.Drawing.Point(3, 267)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(137, 80)
        Me.Button2.TabIndex = 18
        Me.Button2.Text = "Regresar"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.UseVisualStyleBackColor = False
        '
        'cmbnivel
        '
        Me.cmbnivel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmbnivel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbnivel.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbnivel.FormattingEnabled = True
        Me.cmbnivel.Items.AddRange(New Object() {"Bachillerato", "Tercer ciclo", "Segundo ciclo", "Primer ciclo", "Primaria"})
        Me.cmbnivel.Location = New System.Drawing.Point(171, 235)
        Me.cmbnivel.Name = "cmbnivel"
        Me.cmbnivel.Size = New System.Drawing.Size(202, 25)
        Me.cmbnivel.TabIndex = 17
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(137, 31)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Nombre de usuario:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(137, 38)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Contraseña:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(137, 37)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Repetir contraseña:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(137, 43)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Pregunta de seguridad:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 149)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(137, 48)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Respuesta:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 197)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(137, 35)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Tipo de usuario:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtusuario
        '
        Me.txtusuario.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtusuario.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtusuario.Location = New System.Drawing.Point(168, 4)
        Me.txtusuario.Name = "txtusuario"
        Me.txtusuario.Size = New System.Drawing.Size(208, 22)
        Me.txtusuario.TabIndex = 6
        '
        'txtcontra
        '
        Me.txtcontra.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtcontra.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcontra.Location = New System.Drawing.Point(168, 39)
        Me.txtcontra.Name = "txtcontra"
        Me.txtcontra.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtcontra.Size = New System.Drawing.Size(207, 22)
        Me.txtcontra.TabIndex = 7
        '
        'txtrepetircontra
        '
        Me.txtrepetircontra.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtrepetircontra.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtrepetircontra.Location = New System.Drawing.Point(167, 76)
        Me.txtrepetircontra.Name = "txtrepetircontra"
        Me.txtrepetircontra.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtrepetircontra.Size = New System.Drawing.Size(209, 22)
        Me.txtrepetircontra.TabIndex = 8
        '
        'txtrespuesta
        '
        Me.txtrespuesta.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtrespuesta.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtrespuesta.Location = New System.Drawing.Point(167, 162)
        Me.txtrespuesta.Name = "txtrespuesta"
        Me.txtrespuesta.Size = New System.Drawing.Size(209, 22)
        Me.txtrespuesta.TabIndex = 9
        '
        'cmbtipousuario
        '
        Me.cmbtipousuario.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmbtipousuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbtipousuario.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbtipousuario.FormattingEnabled = True
        Me.cmbtipousuario.Items.AddRange(New Object() {"Administrador", "Encargado multimedia", "Coordinador", "Profesor", "Alumno"})
        Me.cmbtipousuario.Location = New System.Drawing.Point(171, 202)
        Me.cmbtipousuario.Name = "cmbtipousuario"
        Me.cmbtipousuario.Size = New System.Drawing.Size(202, 25)
        Me.cmbtipousuario.TabIndex = 11
        '
        'cmbpregunta
        '
        Me.cmbpregunta.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmbpregunta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbpregunta.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbpregunta.FormattingEnabled = True
        Me.cmbpregunta.Items.AddRange(New Object() {"Nombre de tu primera mascota", "Color favorito", "Nombre de tu mejor amigo de la infancia", "Ciudad donde creciste", "Nombre de tu madre o padre"})
        Me.cmbpregunta.Location = New System.Drawing.Point(169, 115)
        Me.cmbpregunta.Name = "cmbpregunta"
        Me.cmbpregunta.Size = New System.Drawing.Size(206, 25)
        Me.cmbpregunta.TabIndex = 10
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label8.Location = New System.Drawing.Point(3, 232)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(137, 32)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Nivel:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnfinalizar
        '
        Me.btnfinalizar.AutoSize = True
        Me.btnfinalizar.BackColor = System.Drawing.Color.Teal
        Me.btnfinalizar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnfinalizar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnfinalizar.Location = New System.Drawing.Point(146, 267)
        Me.btnfinalizar.Name = "btnfinalizar"
        Me.btnfinalizar.Size = New System.Drawing.Size(252, 80)
        Me.btnfinalizar.TabIndex = 19
        Me.btnfinalizar.Text = "Finalizar"
        Me.btnfinalizar.UseVisualStyleBackColor = False
        '
        'CrearUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(407, 379)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "CrearUsuario"
        Me.Text = "CrearUsuario"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtusuario As TextBox
    Friend WithEvents txtcontra As TextBox
    Friend WithEvents txtrepetircontra As TextBox
    Friend WithEvents txtrespuesta As TextBox
    Friend WithEvents cmbtipousuario As ComboBox
    Friend WithEvents cmbpregunta As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents cmbnivel As ComboBox
    Friend WithEvents Button2 As Button
    Friend WithEvents btnfinalizar As Button
End Class
