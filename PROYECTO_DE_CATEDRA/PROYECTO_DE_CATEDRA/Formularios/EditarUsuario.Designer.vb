﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditarUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmbnivelEditar = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtusuarioEditar = New System.Windows.Forms.TextBox()
        Me.txtcontraEditar = New System.Windows.Forms.TextBox()
        Me.txtRepetirContraEditar = New System.Windows.Forms.TextBox()
        Me.txtrespuestaEditar = New System.Windows.Forms.TextBox()
        Me.cmbtipousuarioEditar = New System.Windows.Forms.ComboBox()
        Me.cmbpreguntaEditar = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.762533!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.23746!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(407, 379)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.66085!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.33915!))
        Me.TableLayoutPanel2.Controls.Add(Me.cmbnivelEditar, 1, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.Label8, 0, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.Button2, 0, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.Label2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label3, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label4, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label6, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.Label7, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.txtusuarioEditar, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtcontraEditar, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.txtRepetirContraEditar, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.txtrespuestaEditar, 1, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbtipousuarioEditar, 1, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbpreguntaEditar, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Button1, 1, 7)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 40)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 8
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.90909!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.09091!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(401, 336)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'cmbnivelEditar
        '
        Me.cmbnivelEditar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmbnivelEditar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbnivelEditar.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbnivelEditar.FormattingEnabled = True
        Me.cmbnivelEditar.Items.AddRange(New Object() {"Bachillerato", "Tercer ciclo", "Segundo ciclo", "Primer ciclo", "Primaria"})
        Me.cmbnivelEditar.Location = New System.Drawing.Point(171, 229)
        Me.cmbnivelEditar.Name = "cmbnivelEditar"
        Me.cmbnivelEditar.Size = New System.Drawing.Size(202, 25)
        Me.cmbnivelEditar.TabIndex = 17
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(3, 223)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(137, 37)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Nivel:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Teal
        Me.Button2.BackgroundImage = Global.PROYECTO_DE_CATEDRA.My.Resources.Resources.back_arrow
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button2.Location = New System.Drawing.Point(3, 263)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(137, 70)
        Me.Button2.TabIndex = 15
        Me.Button2.Text = "Regresar"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(137, 29)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Nombre de usuario:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(137, 27)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Contraseña:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(137, 36)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Repetir contraseña:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 92)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(137, 47)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Pregunta de seguridad:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 139)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(137, 44)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Respuesta:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 183)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(137, 40)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Tipo de usuario:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtusuarioEditar
        '
        Me.txtusuarioEditar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtusuarioEditar.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtusuarioEditar.Location = New System.Drawing.Point(168, 3)
        Me.txtusuarioEditar.Name = "txtusuarioEditar"
        Me.txtusuarioEditar.Size = New System.Drawing.Size(208, 22)
        Me.txtusuarioEditar.TabIndex = 6
        '
        'txtcontraEditar
        '
        Me.txtcontraEditar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtcontraEditar.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcontraEditar.Location = New System.Drawing.Point(168, 32)
        Me.txtcontraEditar.Name = "txtcontraEditar"
        Me.txtcontraEditar.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtcontraEditar.Size = New System.Drawing.Size(207, 22)
        Me.txtcontraEditar.TabIndex = 7
        '
        'txtRepetirContraEditar
        '
        Me.txtRepetirContraEditar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtRepetirContraEditar.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRepetirContraEditar.Location = New System.Drawing.Point(167, 63)
        Me.txtRepetirContraEditar.Name = "txtRepetirContraEditar"
        Me.txtRepetirContraEditar.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtRepetirContraEditar.Size = New System.Drawing.Size(209, 22)
        Me.txtRepetirContraEditar.TabIndex = 8
        '
        'txtrespuestaEditar
        '
        Me.txtrespuestaEditar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtrespuestaEditar.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtrespuestaEditar.Location = New System.Drawing.Point(167, 150)
        Me.txtrespuestaEditar.Name = "txtrespuestaEditar"
        Me.txtrespuestaEditar.Size = New System.Drawing.Size(209, 22)
        Me.txtrespuestaEditar.TabIndex = 9
        '
        'cmbtipousuarioEditar
        '
        Me.cmbtipousuarioEditar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmbtipousuarioEditar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbtipousuarioEditar.Enabled = False
        Me.cmbtipousuarioEditar.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbtipousuarioEditar.FormattingEnabled = True
        Me.cmbtipousuarioEditar.Items.AddRange(New Object() {"Coordinador", "Administrador", "Profesor", "Encargado multimedia", "Alumno"})
        Me.cmbtipousuarioEditar.Location = New System.Drawing.Point(171, 190)
        Me.cmbtipousuarioEditar.Name = "cmbtipousuarioEditar"
        Me.cmbtipousuarioEditar.Size = New System.Drawing.Size(202, 25)
        Me.cmbtipousuarioEditar.TabIndex = 11
        '
        'cmbpreguntaEditar
        '
        Me.cmbpreguntaEditar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmbpreguntaEditar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbpreguntaEditar.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbpreguntaEditar.FormattingEnabled = True
        Me.cmbpreguntaEditar.Items.AddRange(New Object() {"Nombre de tu primera mascota", "Color favorito", "Nombre de tu mejor amigo de la infancia", "Ciudad donde creciste", "Nombre de tu madre o padre"})
        Me.cmbpreguntaEditar.Location = New System.Drawing.Point(169, 103)
        Me.cmbpreguntaEditar.Name = "cmbpreguntaEditar"
        Me.cmbpreguntaEditar.Size = New System.Drawing.Size(206, 25)
        Me.cmbpreguntaEditar.TabIndex = 10
        '
        'Button1
        '
        Me.Button1.AutoSize = True
        Me.Button1.BackColor = System.Drawing.Color.Tomato
        Me.Button1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(146, 263)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(252, 70)
        Me.Button1.TabIndex = 18
        Me.Button1.Text = "Finalizar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 5)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(397, 27)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Completa los siguientes datos:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'EditarUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(407, 379)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "EditarUsuario"
        Me.Text = "EditarUsuario"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtusuarioEditar As TextBox
    Friend WithEvents txtcontraEditar As TextBox
    Friend WithEvents txtRepetirContraEditar As TextBox
    Friend WithEvents txtrespuestaEditar As TextBox
    Friend WithEvents cmbtipousuarioEditar As ComboBox
    Friend WithEvents cmbpreguntaEditar As ComboBox
    Friend WithEvents cmbnivelEditar As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
End Class
