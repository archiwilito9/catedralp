﻿Public Class EditarUsuarioDGV
    Dim conectar As New Conexion("localhost", "root", "", "proy_lp_catedra")
    Dim consulta As String = "SELECT * FROM usuarios"
    Dim fEditar, usuarioE As String
    Dim contraE, preguntaE, respuestaE As String
    Dim tipo_usuarioE, nivelE As String

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        GestionarUsuarios.Show()
        Me.Close()
    End Sub

    Private Sub EditarUsuarioDGV_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvEditarUsuarios.Refresh()
        conectar.llenar_dgvEditar(dgvEditarUsuarios, consulta, "usuarios")
    End Sub


    Private Sub dgvEditarUsuarios_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvEditarUsuarios.CellContentClick
        If e.ColumnIndex = 7 Then
            'Obtener información de los usuarios
            'USUARIO
            fEditar = dgvEditarUsuarios.Rows(e.RowIndex).Cells(1).Value.ToString
            usuarioE = conectar.campo("SELECT usuario FROM usuarios WHERE usuario = '" & fEditar & "'")
            contraE = conectar.campo("SELECT contra FROM usuarios WHERE usuario = '" & fEditar & "'")
            preguntaE = conectar.campo("SELECT pregunta FROM usuarios WHERE usuario = '" & fEditar & "'")
            respuestaE = conectar.campo("SELECT respuesta FROM usuarios WHERE usuario = '" & fEditar & "'")
            tipo_usuarioE = conectar.campo("SELECT tipo_usuario FROM usuarios WHERE usuario = '" & fEditar & "'")
            nivelE = conectar.campo("SELECT Nivel FROM usuarios WHERE usuario = '" & fEditar & "'")
            'Enviar información al otro form
            With EditarUsuario
                .txtusuarioEditar.Text = usuarioE
                .txtcontraEditar.Text = contraE
                .txtRepetirContraEditar.Text = contraE
                .cmbpreguntaEditar.SelectedItem = preguntaE
                .txtrespuestaEditar.Text = respuestaE
                .cmbtipousuarioEditar.SelectedItem = tipo_usuarioE
                .cmbnivelEditar.SelectedItem = nivelE
            End With
            EditarUsuario.Show()
            Me.Hide()
        End If
    End Sub
End Class