﻿Public Class EliminarUsuario
    Dim conectar As New Conexion("localhost", "root", "", "proy_lp_catedra")
    Dim consulta As String = "SELECT * FROM usuarios"
    Dim usuarioB, usuarioT As String
    Dim consulta_eliminar As String
    Dim fBorrar, fTipo_Usuario As String
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        GestionarUsuarios.Show()
        Me.Close()
    End Sub

    Private Sub dgvEliminarUsuario_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvEliminarUsuario.CellContentClick
        If e.ColumnIndex = 7 Then
            fBorrar = dgvEliminarUsuario.Rows(e.RowIndex).Cells(1).Value.ToString
            fTipo_Usuario = dgvEliminarUsuario.Rows(e.RowIndex).Cells(5).Value.ToString
            usuarioB = conectar.campo("SELECT id_usuario FROM usuarios WHERE usuario = '" & fBorrar & "'")
            usuarioT = conectar.campo("SELECT id_usuario FROM usuarios WHERE usuario = '" & fTipo_Usuario & "'")
            consulta_eliminar = "DELETE FROM usuarios WHERE id_usuario = '" & usuarioB & "'"
            If MsgBox("Está seguro de eliminar a este usuario?", MsgBoxStyle.YesNo) = vbYes Then
                Try
                    conectar.ejecutar(consulta_eliminar)
                    dgvEliminarUsuario.Rows.RemoveAt(e.RowIndex)
                    If fTipo_Usuario = "Administrador" Then
                        conectar.ejecutar("DELETE FROM administrador WHERE id_usuarios = '" & usuarioB & "'")
                    ElseIf fTipo_Usuario = "Coordinador" Then
                        conectar.ejecutar("DELETE FROM coordinador WHERE id_usuarios = '" & usuarioB & "'")
                    ElseIf fTipo_Usuario = "Profesor" Then
                        conectar.ejecutar("DELETE FROM profesor WHERE id_usuarios = '" & usuarioB & "'")
                    ElseIf fTipo_Usuario = "Encargado multimedia" Then
                        conectar.ejecutar("DELETE FROM encargado WHERE id_usuarios = '" & usuarioB & "'")
                    End If
                    MsgBox("Usuario eliminado correctamente")
                Catch ex As Exception
                    Throw New Exception("El usuario no pudo ser eliminado")
                End Try
            End If
        End If
    End Sub

    Private Sub EliminarUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvEliminarUsuario.Refresh()
        conectar.llenar_dgvEliminar(dgvEliminarUsuario, consulta, "usuarios")
    End Sub
End Class