﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminMenú
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btngetionarequipomultimedia = New System.Windows.Forms.Button()
        Me.btngestionarsalonmultimedia = New System.Windows.Forms.Button()
        Me.btngestionarusuarios = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btngetionarequipomultimedia
        '
        Me.btngetionarequipomultimedia.BackColor = System.Drawing.Color.Teal
        Me.btngetionarequipomultimedia.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btngetionarequipomultimedia.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btngetionarequipomultimedia.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btngetionarequipomultimedia.Location = New System.Drawing.Point(312, 3)
        Me.btngetionarequipomultimedia.Name = "btngetionarequipomultimedia"
        Me.btngetionarequipomultimedia.Size = New System.Drawing.Size(158, 264)
        Me.btngetionarequipomultimedia.TabIndex = 2
        Me.btngetionarequipomultimedia.Text = "Gestionar quipo multimedia"
        Me.btngetionarequipomultimedia.UseVisualStyleBackColor = False
        '
        'btngestionarsalonmultimedia
        '
        Me.btngestionarsalonmultimedia.BackColor = System.Drawing.Color.OrangeRed
        Me.btngestionarsalonmultimedia.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btngestionarsalonmultimedia.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btngestionarsalonmultimedia.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btngestionarsalonmultimedia.Location = New System.Drawing.Point(160, 3)
        Me.btngestionarsalonmultimedia.Name = "btngestionarsalonmultimedia"
        Me.btngestionarsalonmultimedia.Size = New System.Drawing.Size(146, 264)
        Me.btngestionarsalonmultimedia.TabIndex = 1
        Me.btngestionarsalonmultimedia.Text = "Gestionar salón multimedia"
        Me.btngestionarsalonmultimedia.UseVisualStyleBackColor = False
        '
        'btngestionarusuarios
        '
        Me.btngestionarusuarios.BackColor = System.Drawing.Color.Teal
        Me.btngestionarusuarios.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btngestionarusuarios.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btngestionarusuarios.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btngestionarusuarios.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btngestionarusuarios.Location = New System.Drawing.Point(3, 3)
        Me.btngestionarusuarios.Name = "btngestionarusuarios"
        Me.btngestionarusuarios.Size = New System.Drawing.Size(151, 264)
        Me.btngestionarusuarios.TabIndex = 0
        Me.btngestionarusuarios.Text = "Gestionar Usuarios"
        Me.btngestionarusuarios.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.77882!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.22118!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.btngestionarusuarios, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btngestionarsalonmultimedia, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btngetionarequipomultimedia, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Button1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TableLayoutPanel1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.96748!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(473, 310)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.OrangeRed
        Me.Button1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button1.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Button1.Location = New System.Drawing.Point(3, 273)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(151, 34)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Cerrar sesión"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'AdminMenú
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(473, 310)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "AdminMenú"
        Me.Text = "AdminMenú"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btngetionarequipomultimedia As Button
    Friend WithEvents btngestionarsalonmultimedia As Button
    Friend WithEvents btngestionarusuarios As Button
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Button1 As Button
End Class
