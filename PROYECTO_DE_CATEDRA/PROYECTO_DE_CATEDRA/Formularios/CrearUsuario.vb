﻿Public Class CrearUsuario
    Dim comando, validando As String
    Dim validar As New Validaciones
    Dim conectando As New Conexion("localhost", "root", "", "proy_lp_catedra")

    Private Sub limpiadoras()
        validar.LimpiarTextBox(txtusuario)
        validar.LimpiarTextBox(txtcontra)
        validar.LimpiarTextBox(txtrespuesta)
        validar.LimpiarTextBox(txtrepetircontra)
        'validar.LimpiarComboBox(cmbnivel)
        'validar.LimpiarComboBox(cmbpregunta)
        'validar.LimpiarComboBox(cmbtipousuario)
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnfinalizar.Click
        Try
            validar.AsignarUsuario = txtusuario.Text.Trim
            validar.AsignarContra = txtcontra.Text.Trim
            validar.AsignarRepetirContra = txtrepetircontra.Text.Trim
            validar.AsignarPregunta = cmbpregunta.SelectedItem
            validar.AsignarRespuesta = txtrespuesta.Text.Trim
            validar.AsignarTipoUsuario = cmbtipousuario.Text.Trim
            comando = "SELECT COUNT(*) FROM usuarios WHERE usuario = '" & txtusuario.Text.Trim & "'"
            If cmbtipousuario.SelectedItem = "Alumno" Then
                If validar.ValidarUsuario() And validar.ValidarComboBox(cmbtipousuario) And validar.ValidarComboBox(cmbnivel) Then
                    Try
                        If conectando.campo(comando) = "0" Then
                            conectando.ejecutar("INSERT INTO usuarios (usuario, tipo_usuario,Nivel) VALUES ('" & txtusuario.Text & "', '" & cmbtipousuario.SelectedItem & "', '" & cmbnivel.SelectedItem & "')")
                            validando = conectando.campo("SELECT id_usuario FROM usuarios WHERE usuario = '" & txtusuario.Text.Trim & "'")
                            conectando.ejecutar("INSERT INTO alumno (id_usuarios,nombre,nivel) VALUES ('" & validando & "', '" & cmbtipousuario.SelectedItem & "', '" & cmbnivel.SelectedItem & "')")
                            MsgBox("Usuario creado correctamente!", MsgBoxStyle.Information)
                        Else
                            MsgBox("El usuario ya está registrado", MsgBoxStyle.Critical)
                        End If
                    Catch ex As Exception
                        Throw New Exception("Hubo un error al crear al usuario")
                    End Try
                End If
            Else
                If validar.ValidarUsuario() And validar.ValidarContra() And validar.ValidarRespuesta() And validar.ValidarComboBox(cmbnivel, cmbpregunta, cmbtipousuario) Then
                    If conectando.campo(comando) = "0" Then
                        conectando.ejecutar("INSERT INTO usuarios (usuario,contra,pregunta,respuesta,tipo_usuario,Nivel) VALUES ('" & txtusuario.Text & "', '" & txtcontra.Text & "', '" & cmbpregunta.SelectedItem & "', '" & txtrespuesta.Text.Trim & "', '" & cmbtipousuario.SelectedItem & "', '" & cmbnivel.SelectedItem & "')")
                        If cmbtipousuario.SelectedItem = "Administrador" Then
                            validando = conectando.campo("SELECT id_usuario FROM usuarios WHERE usuario = '" & txtusuario.Text.Trim & "'")
                            conectando.ejecutar("INSERT INTO administrador (id_usuarios) VALUES ('" & validando & "')")
                            limpiadoras()
                            MensajeRegistro()
                        ElseIf cmbtipousuario.SelectedItem = "Coordinador" Then
                            validando = conectando.campo("SELECT id_usuario FROM usuarios WHERE usuario = '" & txtusuario.Text.Trim & "'")
                            conectando.ejecutar("INSERT INTO coordinador (id_usuarios) VALUES ('" & validando & "')")
                            limpiadoras()
                            MensajeRegistro()
                        ElseIf cmbtipousuario.SelectedItem = "Profesor" Then
                            validando = conectando.campo("SELECT id_usuario FROM usuarios WHERE usuario = '" & txtusuario.Text.Trim & "'")
                            conectando.ejecutar("INSERT INTO profesor (id_usuarios) VALUES ('" & validando & "')")
                            limpiadoras()
                            MensajeRegistro()
                        ElseIf cmbtipousuario.SelectedItem = "Encargado multimedia" Then
                            validando = conectando.campo("SELECT id_usuario FROM usuarios WHERE usuario = '" & txtusuario.Text.Trim & "'")
                            conectando.ejecutar("INSERT INTO encargado (id_usuarios) VALUES ('" & validando & "')")
                            limpiadoras()
                            MensajeRegistro()
                        End If
                    Else
                        MsgBox("El usuario ya está registrado!", MsgBoxStyle.Critical)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception("Hubo un error al crear al usuario")
        End Try
    End Sub
    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        GestionarUsuarios.Show()
        Me.Close()
    End Sub

    Private Sub cmbtipousuario_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbtipousuario.SelectedIndexChanged
        If cmbtipousuario.SelectedItem = "Alumno" Then
            Label3.Text = ""
            txtcontra.Visible = False
            Label4.Text = ""
            txtrepetircontra.Visible = False
            Label5.Text = ""
            cmbpregunta.Visible = False
            Label6.Text = ""
            txtrespuesta.Visible = False
        Else
            Label3.Text = "Contraseña"
            txtcontra.Visible = True
            Label4.Text = "Repetir contraseña:"
            txtrepetircontra.Visible = True
            Label5.Text = "Pregunta de seguridad:"
            cmbpregunta.Visible = True
            Label6.Text = "Respuesta:"
            txtrespuesta.Visible = True
        End If
    End Sub

    Private Sub MensajeRegistro()
        MsgBox("Usuario registrado correctamente")
    End Sub
End Class