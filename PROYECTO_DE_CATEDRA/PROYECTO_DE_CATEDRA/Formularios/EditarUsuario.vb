﻿Public Class EditarUsuario
    Dim conectar As New Conexion("localhost", "root", "", "proy_lp_catedra")
    Dim validar As New Validaciones
    Dim usuario As String
    Dim contra As String
    Dim pregunta As String
    Dim respuesta As String
    Dim tipo_usuario As String
    Dim nivel As String
    Dim comando As String

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        EditarUsuarioDGV.Show()
        Me.Hide()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        usuario = txtusuarioEditar.Text.Trim
        contra = txtcontraEditar.Text.Trim
        pregunta = cmbpreguntaEditar.SelectedItem
        respuesta = txtrespuestaEditar.Text.Trim
        tipo_usuario = cmbtipousuarioEditar.SelectedItem
        nivel = cmbnivelEditar.SelectedItem
        comando = "SELECT COUNT(*) FROM usuarios WHERE usuario = '" & usuario & "'"
        Try
            If validar.ValidarUsuario() And validar.ValidarContra And validar.ValidarRespuesta = True And validar.ValidarComboBox(cmbnivelEditar, cmbpreguntaEditar, cmbtipousuarioEditar) Then
                If conectar.campo(comando) = "0" Then
                    conectar.ejecutar("UPDATE usuarios SET usuario = '" & usuario & "', contra = '" & contra & "', pregunta = '" & pregunta & "', respuesta = '" & respuesta & "', tipo_usuario = '" & tipo_usuario & "', Nivel = '" & nivel & "'")
                    MsgBox("El usuario fue editado correctamente", MsgBoxStyle.Information)
                Else
                    MsgBox("El usuario ya existe", MsgBoxStyle.Critical)
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub EditarUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class