﻿Public Class AsignarEquipoMultimedia
    Dim conectar As New Conexion("localhost", "root", "", "proy_lp_catedra")
    Dim validar As New Validaciones
    Dim hora_inicio As String = "7:00 AM", hora_fin As String = "6:00 PM"
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        GestionarEquipoMultimedia.Show()
        Me.Hide()
    End Sub

    Private Sub AsignarEquipoMultimedia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpinicio.Value = Convert.ToDateTime(hora_inicio)
        dtpfinal.Value = Convert.ToDateTime(hora_inicio)
        dtpinicio.MinDate = Convert.ToDateTime(hora_inicio)
        dtpfinal.MinDate = Convert.ToDateTime(hora_inicio)
        dtpfinal.MaxDate = Convert.ToDateTime(hora_fin)
        dtpinicio.MaxDate = Convert.ToDateTime(hora_fin)
        dtpfecha.Value = Today
        dtpfecha.MinDate = Today
        conectar.llenar_cbo(ComboBox1, "SELECT usuario FROM usuarios", "usuarios")
        conectar.llenar_cbo(ComboBox2, "SELECT nombre FROM equipo", "equipo")
    End Sub

    Private Sub DateTimePicker2_KeyDown(sender As Object, e As KeyEventArgs) Handles dtpinicio.KeyDown
        validar.ValidarHoras(dtpinicio, e)
    End Sub
    Private Sub DateTimePicker1_KeyDown(sender As Object, e As KeyEventArgs) Handles dtpfecha.KeyDown
        validar.ValidarHoras(dtpfecha, e)
    End Sub
End Class