﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GestionarUsuarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.btncrearusuario = New System.Windows.Forms.Button()
        Me.btneditarusuario = New System.Windows.Forms.Button()
        Me.btneliminarusuario = New System.Windows.Forms.Button()
        Me.btnregresar = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.btncrearusuario, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btneditarusuario, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btneliminarusuario, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btnregresar, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(427, 326)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'btncrearusuario
        '
        Me.btncrearusuario.BackColor = System.Drawing.Color.Coral
        Me.btncrearusuario.BackgroundImage = Global.PROYECTO_DE_CATEDRA.My.Resources.Resources.create_group_button
        Me.btncrearusuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btncrearusuario.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btncrearusuario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btncrearusuario.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btncrearusuario.Location = New System.Drawing.Point(5, 5)
        Me.btncrearusuario.Margin = New System.Windows.Forms.Padding(5)
        Me.btncrearusuario.Name = "btncrearusuario"
        Me.btncrearusuario.Size = New System.Drawing.Size(203, 153)
        Me.btncrearusuario.TabIndex = 0
        Me.btncrearusuario.Text = "Crear Usuario"
        Me.btncrearusuario.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btncrearusuario.UseVisualStyleBackColor = False
        '
        'btneditarusuario
        '
        Me.btneditarusuario.BackColor = System.Drawing.Color.Coral
        Me.btneditarusuario.BackgroundImage = Global.PROYECTO_DE_CATEDRA.My.Resources.Resources.writing
        Me.btneditarusuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btneditarusuario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btneditarusuario.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btneditarusuario.Location = New System.Drawing.Point(218, 5)
        Me.btneditarusuario.Margin = New System.Windows.Forms.Padding(5)
        Me.btneditarusuario.Name = "btneditarusuario"
        Me.btneditarusuario.Size = New System.Drawing.Size(204, 153)
        Me.btneditarusuario.TabIndex = 1
        Me.btneditarusuario.Text = "Editar Usuario"
        Me.btneditarusuario.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btneditarusuario.UseVisualStyleBackColor = False
        '
        'btneliminarusuario
        '
        Me.btneliminarusuario.BackColor = System.Drawing.Color.Coral
        Me.btneliminarusuario.BackgroundImage = Global.PROYECTO_DE_CATEDRA.My.Resources.Resources.delete
        Me.btneliminarusuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btneliminarusuario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btneliminarusuario.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btneliminarusuario.Location = New System.Drawing.Point(5, 168)
        Me.btneliminarusuario.Margin = New System.Windows.Forms.Padding(5)
        Me.btneliminarusuario.Name = "btneliminarusuario"
        Me.btneliminarusuario.Size = New System.Drawing.Size(203, 153)
        Me.btneliminarusuario.TabIndex = 2
        Me.btneliminarusuario.Text = "Eliminar Usuario"
        Me.btneliminarusuario.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btneliminarusuario.UseVisualStyleBackColor = False
        '
        'btnregresar
        '
        Me.btnregresar.BackColor = System.Drawing.Color.Teal
        Me.btnregresar.BackgroundImage = Global.PROYECTO_DE_CATEDRA.My.Resources.Resources.back_arrow
        Me.btnregresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnregresar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnregresar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnregresar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnregresar.Location = New System.Drawing.Point(218, 168)
        Me.btnregresar.Margin = New System.Windows.Forms.Padding(5)
        Me.btnregresar.Name = "btnregresar"
        Me.btnregresar.Size = New System.Drawing.Size(204, 153)
        Me.btnregresar.TabIndex = 3
        Me.btnregresar.Text = "Regresar al menú anterior"
        Me.btnregresar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnregresar.UseVisualStyleBackColor = False
        '
        'GestionarUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(427, 326)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "GestionarUsuarios"
        Me.Text = "GestionarUsuarios"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents btncrearusuario As Button
    Friend WithEvents btneliminarusuario As Button
    Friend WithEvents btnregresar As Button
    Friend WithEvents btneditarusuario As Button
End Class
